"""Application tezt module."""

import subprocess
import sys
from pathlib import Path
from unittest import TestCase


class AppTestCase(TestCase):
    def setUp(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "install",
                Path(__file__).parent / "dummy",
            ],
        )

    def tearDown(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "uninstall",
                "-y",
                "dummy",
            ],
        )

    def test_app(self):
        galactic = subprocess.run(
            ["galactic"],
            capture_output=True,
            text=True,
            check=False,
        )
        galactic_output = galactic.stdout.strip()
        expected_output = r"""
Usage: galactic [OPTIONS] COMMAND [ARGS]...

  GAlois LAttices, Concept Theory, Implicational systems and Closures.

Options:
  --version             Show the version and exit.
  --color / --no-color  Whether or not to display colored output (default is
                        auto-detection) [env vars: `FORCE_COLOR`/`NO_COLOR`]
  --help                Show this message and exit.

Commands:
  dummy  Echo dummy.

  Main GALACTIC command
        """.strip()

        self.maxDiff = None
        self.assertMultiLineEqual(galactic_output, expected_output)
