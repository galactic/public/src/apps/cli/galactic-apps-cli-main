"""
Dummy module.
"""

import click

import galactic.apps.cli.main as cli


@click.command()
def dummy() -> int:
    """
    Echo dummy.
    """
    click.echo("Dummy")
    return 0


def register() -> None:
    """
    Register plugin.
    """
    cli.application.add_command(dummy)
