"""
main apps core module.
"""

__all__ = (
    "application",
    "error",
    "info",
    "warning",
    "comment",
    "prompt",
    "confirm",
    "AppStyles",
)

from ._app import AppStyles, application, comment, confirm, error, info, prompt, warning
