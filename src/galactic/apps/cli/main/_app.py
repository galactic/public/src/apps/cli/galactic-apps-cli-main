"""
Main app module.
"""

from __future__ import annotations

import os
from typing import Any, ClassVar

import click

from galactic.helpers.core import detect_plugins


# pylint: disable=too-few-public-methods
class AppStyles:
    """
    Styles used in error, info, warning, comment, prompt and confirm.
    """

    ERROR: ClassVar[dict[str, Any]] = {"fg": "red", "bold": True}
    WARNING: ClassVar[dict[str, Any]] = {"fg": "yellow", "bold": True}
    INFO: ClassVar[dict[str, Any]] = {"fg": "bright_magenta"}
    COMMENT: ClassVar[dict[str, Any]] = {"fg": "green"}
    QUESTION: ClassVar[dict[str, Any]] = {"fg": "cyan"}


def error(text: str, **kwargs: Any) -> None:
    """
    Display an error message.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.secho

    """
    click.secho(text, **AppStyles.ERROR, **kwargs)


def info(text: str, **kwargs: Any) -> None:
    """
    Display an info message.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.secho

    """
    click.secho(text, **AppStyles.INFO, **kwargs)


def warning(text: str, **kwargs: Any) -> None:
    """
    Display a warning.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.secho

    """
    click.secho(text, **AppStyles.WARNING, **kwargs)


def comment(text: str, **kwargs: Any) -> None:
    """
    Display a commend.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.secho

    """
    click.secho(text, **AppStyles.COMMENT, **kwargs)


def prompt(text: str, **kwargs: Any) -> Any:
    """
    Prompt the user.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.prompt

    """
    return click.prompt(click.style(text, **AppStyles.QUESTION), **kwargs)


def confirm(text: str, **kwargs: Any) -> Any:
    """
    Prompt the user for a confirmation.

    Parameters
    ----------
    text
        Text to display
    **kwargs
        Additional parameters passed to click.confirm

    """
    return click.confirm(click.style(text, **AppStyles.QUESTION), **kwargs)


# pylint: disable=too-few-public-methods
class AppEnvVars:
    """
    Environment variables.
    """

    # https://no-color.org
    NO_COLOR = "NO_COLOR"
    FORCE_COLOR = "FORCE_COLOR"


class Group(click.Group):
    """
    Detect plugins from the galactic.apps.cli.main group.
    """

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._detected = False

    def detect_plugin(self) -> None:
        """
        Detect plugins.
        """
        if not self._detected:
            detect_plugins(group="galactic.apps.cli.main")
            self._detected = True

    def list_commands(self, ctx: click.Context) -> list[str]:
        self.detect_plugin()
        return super().list_commands(ctx)

    def get_command(self, ctx: click.Context, cmd_name: str) -> click.Command | None:
        self.detect_plugin()
        return super().get_command(ctx, cmd_name)


@click.option(
    "--color/--no-color",
    default=None,
    help=(
        "Whether or not to display colored output "
        "(default is auto-detection) [env vars: `FORCE_COLOR`/`NO_COLOR`]"
    ),
)
@click.version_option(package_name="galactic-apps-cli-main")
@click.group(name="galactic", cls=Group, epilog="Main GALACTIC command")
@click.pass_context
def application(ctx: click.Context, color: bool | None = None) -> int:
    """
    GAlois LAttices, Concept Theory, Implicational systems and Closures.
    """
    if color is None:
        if os.environ.get(AppEnvVars.NO_COLOR) == "1":
            color = False
        elif os.environ.get(AppEnvVars.FORCE_COLOR) == "1":
            color = True
    ctx.color = color
    return 0
